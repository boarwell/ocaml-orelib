let contains s substr =
  let len = String.length substr in
  let rec search i =
    if i > String.length s - len then false
    else if String.sub s i len = substr then true
    else search (i + 1)
  in
  search 0


let count s substr =
  let len = String.length substr in
  let rec f i c =
    if i > String.length s - len then c
    else if String.sub s i len = substr then f (i + 1) (c + 1)
    else f (i + 1) c
  in
  f 0 0


let has_prefix s prefix =
  let len = String.length prefix in
  try if String.sub s 0 len = prefix then true else false with _ -> false


let has_suffix s suffix =
  let len_suf = String.length suffix in
  let len_s = String.length s in
  try if String.sub s (len_s - len_suf) len_suf = suffix then true else false
  with _ -> false


(*
let index s substr =
  let n = String.length substr in
  let len_s = String.length s in
  match n with
  | 0 -> 0
  | 1 -> String.index s (*string -> charができなかった*)
  | n' when n' = len_s -> if (s = substr) then 0 else -1
  | n' when n' > len_s -> -1
  |
*)

let repeat s count =
  let rec f tmp i = if i <= 1 then tmp else f (tmp ^ s) (i - 1) in
  f s count


let trim_prefix s prefix =
  let len_pre = String.length prefix in
  let len_s = String.length s in
  if has_prefix s prefix then String.sub s len_pre (len_s - len_pre) else s


let trim_suffix s suffix =
  if has_suffix s suffix then
    let len_suf = String.length suffix in
    let len_s = String.length s in
    String.sub s 0 (len_s - len_suf)
  else s


let () =
  Printf.printf "%b\n" (contains "fhogeuga" "hoge") ;
  Printf.printf "%d\n" (count "ssisnotss" "ss") ;
  Printf.printf "%b\n" (has_prefix "ssisnotss" "safasfasfasdfs") ;
  Printf.printf "%b\n" (has_suffix "hoge" "ge") ;
  Printf.printf "%s\n" (repeat "hoge" 2) ;
  Printf.printf "%s\n" (trim_prefix "ssis" "sas") ;
  Printf.printf "%s\n" (trim_suffix "ssis" "is") ;

