let list_go ls =
  let rec aux tmp = function
    | [] -> ""
    | [x] -> tmp ^ ", " ^ x ^ "]"
    | h :: t -> aux (tmp ^ ", " ^ h) t
  in
  aux ("[" ^ List.hd ls) (List.tl ls)

