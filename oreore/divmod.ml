let divmod dividend divisor = (dividend / divisor, dividend mod divisor)

let () = Printf.printf "%d\n" (divmod 12 8 |> snd)
